@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/star-rating/lib/jquery.raty.css') !!}
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->
@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			View Driver <small>form...</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">View Driver</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">Action</a>
							</li>
							<li>
								<a href="#">Another action</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">Separated link</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>View Driver
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form class="form-horizontal">
								<div class="form-body">
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									<h3 class="form-section">Personal Info</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Name <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->driver_name }}</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Mobile No <span class="required">
												* </span>
												</label>`
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->mobile_no }}</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Blood Group <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													{!! Form::select('blood_group', [
														'O_+' => 'O +',
														'O_-' => 'O -',
														'A_+' => 'A +',
														'A_-' => 'A -',
														'B_+' => 'B +',
														'B_-' => 'B -',
														'AB_+' => 'AB +',
														'AB_-' => 'AB -'
															], $driver->blood_group, ['class' => 'select2_category form-control', 'data-placeholder' => 'Choose a Category', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Emergency Details <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->emergency_details }}</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Transporter Number <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->transporter_number }}</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Status <span class="required">
												* </span>
												</label>
												<div class="col-md-8"> 
													{!! Form::select('status', [
														'approved' 	  => 'Approved',
														'blacklisted' => 'Blacklisted',
															], $driver->status, ['class' => 'select2_category form-control', 'data-placeholder' => 'Choose a status', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Driver Rating <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<div style="margin-top: 6px;" class="star-rating"></div>
												</div>
											</div>
										</div>										
									</div>
									<h3 class="form-section">License Details</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">License Number <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->driver_license_number }}</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Location <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->driver_license_location}}</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Issue Date <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->driver_license_issue_date }}</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Renewal Date <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->driver_license_renewal_date }}</p>
												</div>
											</div>
										</div>
									</div>
									<h3 class="form-section">Passport Details</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Passport Number <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->passport_number }}</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Name of Authorizers <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->passport_authorizers }}</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">JDE Expiry Date <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->passport_jde_expiry_date }}</p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Expiry Criteria <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<p class="form-control-static">{{ $driver->passport_expiry_criteria }}</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<a href="/drivers" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
@stop

@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}
{!! HTML::script('/assets/admin/global/plugins/ckeditor/ckeditor.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/lib/markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/star-rating/lib/jquery.raty.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-validation.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-samples.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
   	FormValidation.init();
   	FormSamples.init();

   	$('div.star-rating').raty({ score: {{ $driver->driver_rating }} });
});
</script>
<!-- END JAVASCRIPTS -->
@stop