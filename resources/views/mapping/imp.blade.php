@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/star-rating/lib/jquery.raty.css') !!}
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->
@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			<div class="theme-panel hidden-xs hidden-sm">
				<div class="toggler">
				</div>
				<div class="toggler-close">
				</div>
				<div class="theme-options">
					<div class="theme-option theme-colors clearfix">
						<span>
						THEME COLOR </span>
						<ul>
							<li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default">
							</li>
							<li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue">
							</li>
							<li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue">
							</li>
							<li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey">
							</li>
							<li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light">
							</li>
							<li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2">
							</li>
						</ul>
					</div>
					<div class="theme-option">
						<span>
						Layout </span>
						<select class="layout-option form-control input-sm">
							<option value="fluid" selected="selected">Fluid</option>
							<option value="boxed">Boxed</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Header </span>
						<select class="page-header-option form-control input-sm">
							<option value="fixed" selected="selected">Fixed</option>
							<option value="default">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Top Menu Dropdown</span>
						<select class="page-header-top-dropdown-style-option form-control input-sm">
							<option value="light" selected="selected">Light</option>
							<option value="dark">Dark</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Mode</span>
						<select class="sidebar-option form-control input-sm">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Menu </span>
						<select class="sidebar-menu-option form-control input-sm">
							<option value="accordion" selected="selected">Accordion</option>
							<option value="hover">Hover</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Style </span>
						<select class="sidebar-style-option form-control input-sm">
							<option value="default" selected="selected">Default</option>
							<option value="light">Light</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Position </span>
						<select class="sidebar-pos-option form-control input-sm">
							<option value="left" selected="selected">Left</option>
							<option value="right">Right</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Footer </span>
						<select class="page-footer-option form-control input-sm">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
				</div>
			</div>
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Mapping <small>form...</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Map</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">Action</a>
							</li>
							<li>
								<a href="#">Another action</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">Separated link</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Map
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="/drivers" id="driver" method="POST" class="form-horizontal">
								<input type="hidden" name="driver_id">
								<input type="hidden" name="assistant_id">
								<div class="form-body">
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									<div class="scan_status alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										<span class="scan_msg"></span>
									</div>
									<!-- <h3 class="form-section">Personal Info</h3> -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="control-label col-md-4">
												<button type="button" id="assign_driver" class="btn btn-primary green">
														<span class="md-click-circle md-click-animate"></span>Assign Driver
													</button>
												</div>
												<p class="form-control-static">
													<input type="text" name="driver_name" data-required="1" class="form-control" readonly="" />
												</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="control-label col-md-4">
												<button type="button" id="assign_assistant" class="btn btn-primary green">
														<span class="md-click-circle md-click-animate"></span>Assign Assistant
													</button>
												</div>
												<p class="form-control-static">
													<input type="text" name="assistant_name" data-required="1" class="form-control" readonly="" />
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<a href="/drivers" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
@stop

@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}
{!! HTML::script('/assets/admin/global/plugins/ckeditor/ckeditor.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/lib/markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/star-rating/lib/jquery.raty.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-validation.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-samples.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
   	FormValidation.init();
   	FormSamples.init();

   	$("#assign_driver").click(function(){
	   	$(".fixed").css("display", "block");

   		$.ajax({
		  method: "GET",
		  url: "http://127.0.0.1:88/identify.php",
		  // async: false
		}).done(function( msg ) {
		    $("input[name='driver_id']").val(msg);

		    $.ajax({
			  method: "GET",
			  url: "/get/user/"+msg+"/driver",
			  // async: false
			}).done(function( msg ) {
			    if(msg.length == 0) {
					$(".scan_status").css("display", "block");
					$(".scan_msg").html("Invalid User");
				    $(".fixed").css("display", "none");
				    return
				}

				if(msg.status == 0){
						$(".scan_status").css("display", "block");
						$(".scan_msg").html("Driver is Blacklisted");
					    $(".fixed").css("display", "none");
					}else{
						$(".scan_status").css("display", "none");
						$("input[name='driver_name']").val(msg.driver_name);
					    $(".fixed").css("display", "none");
				}
			});
		});
   	});


   	$("#assign_assistant").click(function(){ 
   		$(".fixed").css("display", "block");

   		$.ajax({
		  method: "GET",
		  url: "http://127.0.0.1:88/identify.php",
		  // async: false
		}).done(function( msg ) {
		    $("input[name='assistant_id']").val(msg);
		    $.ajax({
			  method: "GET",
			  url: "/get/user/"+msg+"/assistant",
			  // async: false
			}).done(function( msg ) {
				
				if(msg.length == 0) {
					$(".scan_status").css("display", "block");
					$(".scan_msg").html("Invalid User");
				    $(".fixed").css("display", "none");
				    return
				}

				if(msg.status == 0){
						$(".scan_status").css("display", "block");
						$(".scan_msg").html("Assistant is Blacklisted");
					    $(".fixed").css("display", "none");
					}else{
						$(".scan_status").css("display", "none");
						$("input[name='assistant_name']").val(msg.assistant_name);
					    $(".fixed").css("display", "none");
				}

			  });
		  });

   		
   	});

});
</script>
<!-- END JAVASCRIPTS -->
@stop