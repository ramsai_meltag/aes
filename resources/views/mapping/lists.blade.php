@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
{!! HTML::style('/assets/admin/global/plugins/nifty-modal/css/component.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->
@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<button style="visibility:hidden" class="md-trigger" data-modal="modal-14"></button>

	<div class="md-modal md-effect-14" id="modal-14">
		<div class="md-content">
			<h3>Alert</h3>
			<div class="dialog-msg"></div>
			<div>
				<button type="button" class="btn btn-primary blue md-close">
					<span class="md-click-circle md-click-animate"></span>Close
				</button>
			</div>
		</div>
	</div>


	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Logout</h4>
						</div>
						<input type="hidden" name="modal_driver_id">
						<input type="hidden" name="modal_assistant_id">
						<input type="hidden" name="modal_launch_trip_id">
						<div class="modal-body">
							<div class="form-group">
								<div class="control-label col-md-4">
								<button type="button" id="assign_driver" class="btn btn-primary green">
										<span class="md-click-circle md-click-animate"></span>Scan Driver
									</button>
								</div>
								<p class="form-control-static">
									<input type="text" style="margin-top: -8px;" name="driver_name" data-required="1" class="form-control" readonly="" />
									<input type="hidden" name="driver_id">
								</p>
							</div>


							<div class="form-group">
								<div class="control-label col-md-4">
								<button type="button" id="assign_assistant" class="btn btn-primary green">
										<span class="md-click-circle md-click-animate"></span>Scan Assistant
									</button>
								</div>
								<p class="form-control-static">
									<input type="text" style="margin-top: -8px;" name="assistant_name" data-required="1" class="form-control" readonly="" />
									<input type="hidden" name="assistant_id">
								</p>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue logout_btn" disabled="">Logout</button>
							<button type="button" class="btn default model-btn-close" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Trips Launched <small>List of Trips Launched</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Trips Launched</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->

			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					@if(Session::has('message'))
					<div class="alert alert-success display-hide" style="display: block;">
						<button class="close" data-close="alert"></button>
						{{ Session::get('message') }}
					</div>
					@endif
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Manage Trips Launched
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href="/mapping/create" id="sample_editable_1_new" class="btn purple">
											Launch Trip <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="btn-group pull-right">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
													Save as PDF </a>
												</li>
												<li>
													<a href="javascript:;">
													Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th>
									 Trip Selected
								</th>
								<th>
									 Driver Assigned
								</th>
								<th>
									 Assistant Assigned
								</th>
								<th>
									 Truck Selected
								</th>
								<th>
									Status
								</th>
								<th>
									Action
								</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach($completeData as $value) { ?>
								<tr class="odd gradeX">
									<td>
										 {{ $value['trip_name'] }}
									</td>
									<td>
										 {{ $value['driver_name'] }}
									</td>
									<td>
										{{ $value['assistant_name'] }}
									</td>
									<td>
										{{ $value['truck_registration_number'] }}
									</td>
									<td>
										@if($value['status'] == 1)
											<span class="label label-sm label-success"> Completed </span>
										@else
											<span class="label label-sm label-warning"> In Transit </span>
										@endif
									</td>
									<td>
										<input type="hidden" name="list_driver_id" value="{{ $value['driver_id'] }}">
										<input type="hidden" name="list_assistant_id" value="{{ $value['assistant_id'] }}">
										<input type="hidden" name="list_launch_trip_id" value="{{ $value['id'] }}">
										<a href="#portlet-config" id="view" data-toggle="modal" class="btn default btn-xs green-stripe"> End Trip </button>	
									</td>
								</tr>
							<?php } ?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->

	<!-- <input type="hidden" name="driver_id">
	<input type="hidden" name="assistant_id"> -->
@stop


@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootbox/bootbox.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/nifty-modal/js/classie.js') !!}
{!! HTML::script('/assets/admin/global/plugins/nifty-modal/js/modalEffects.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/table-managed.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/ui-alert-dialog-api.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   TableManaged.init();
   UIAlertDialogApi.init();

   	$("#view").live('click', function(){
   		console.log("asf");
   		$("input[name='modal_driver_id']").val($(this).parent().find("input[name='list_driver_id']").val());
   		$("input[name='modal_assistant_id']").val($(this).parent().find("input[name='list_assistant_id']").val());
   		$("input[name='modal_launch_trip_id']").val($(this).parent().find("input[name='list_launch_trip_id']").val());
   	});

    $("#assign_driver").click(function(){
	   	$(".fixed").css("display", "block");

   		$.ajax({
		  method: "GET",
		  url: "http://127.0.0.1:88/identify.php",
		  // async: false
		}).done(function( msg ) {
		    $("input[name='driver_id']").val(msg);

		    $.ajax({
			  method: "GET",
			  url: "/get/user/"+msg+"/driver",
			  // async: false
			}).done(function( msg ) {
			    if(msg.length == 0) {
					$(".md-trigger").click();
					$(".dialog-msg").html("Invalid User");
				    $(".fixed").css("display", "none");
				    return
				}

				if(msg.status == 0){
						$(".md-trigger").click();
						$(".dialog-msg").html("Driver is Blacklisted");
					    $(".fixed").css("display", "none");
					}else{
						$(".scan_status").css("display", "none");
						$("input[name='driver_name']").val(msg.driver_name);
					    $(".fixed").css("display", "none");
				}

				console.log($("input[name='modal_driver_id']").val()+" - "+$("input[name='driver_id']").val());
				if($("input[name='modal_driver_id']").val() == $("input[name='driver_id']").val()){

				}else{
					$(".md-trigger").click();
					$(".dialog-msg").html("Unauthorized Driver");
				}
			});
		});
   	});


   	$("#assign_assistant").click(function(){ 
   		$(".fixed").css("display", "block");

   		$.ajax({
		  method: "GET",
		  url: "http://127.0.0.1:88/identify.php",
		  // async: false
		}).done(function( msg ) {
		    $("input[name='assistant_id']").val(msg);
		    $.ajax({
			  method: "GET",
			  url: "/get/user/"+msg+"/assistant",
			  // async: false
			}).done(function( msg ) {
				
				if(msg.length == 0) {
					$(".md-trigger").click();
					$(".dialog-msg").html("Invalid User");
				    $(".fixed").css("display", "none");
				    return
				}

				if(msg.status == 0){
						$(".md-trigger").click();
						$(".dialog-msg").html("Assistant is Blacklisted");
					    $(".fixed").css("display", "none");
					}else{
						$(".scan_status").css("display", "none");
						$("input[name='assistant_name']").val(msg.assistant_name);
					    $(".fixed").css("display", "none");
				}

				console.log($("input[name='modal_assistant_id']").val()+" - "+$("input[name='assistant_id']").val());

				if($("input[name='modal_assistant_id']").val() == $("input[name='assistant_id']").val()){
					$(".logout_btn").removeAttr("disabled");
				}else{
					$(".md-trigger").click();
					$(".dialog-msg").html("Unauthorized Assistant");
				}


			  });
		  });
   	});


	$(".logout_btn").click(function(){
		$.ajax({
		  method: "post",
		  url: "/change/trip/status/"+$("input[name='modal_launch_trip_id']").val()
		}).done(function( msg ) {
			$(".model-btn-close").click();
			location.reload();
		});
	})
});
</script>
<!-- END JAVASCRIPTS -->
@stop
