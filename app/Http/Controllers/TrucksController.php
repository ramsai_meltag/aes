<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Input, Redirect;

class TrucksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $trucks = DB::table('trucks')->orderBy('id', 'desc')->get();
        return View('trucks.lists', compact('trucks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View('trucks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data                   = Input::all();
        $data['created_at']     = date('Y-m-d H:i:s');
        $data['updated_at']     = date('Y-m-d H:i:s');

        DB::table('trucks')
                    ->insert($data);

        return Redirect::to('trucks')
                            ->with('message', 'Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $truck = DB::table('trucks')->where('id', $id)->first();
        return View('trucks.view', compact('truck'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $truck = DB::table('trucks')->where('id', $id)->first();
        return View('trucks.edit', compact('truck'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data                   = Input::except(['score', '_method', '_token']);
        $data['truck_rating']  = Input::get('score');
        $data['updated_at']     = date('Y-m-d H:i:s');

        DB::table('trucks')
                    ->where('id', $id)
                    ->update($data);

        return Redirect::to('trucks')
                            ->with('message', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
