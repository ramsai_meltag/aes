<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Input;
class MappingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = DB::table('mapping')
                        ->select('*')
                        ->get();

        $completeData = [];

        foreach ($data as $key => $value) 
        {
            $driverData = DB::table('drivers')->select('drivers.driver_name', 'users.id')->join('users', 'drivers.user_id', '=', 'users.id')->where('users.id', $value->driver_id)->first();
            $assistantData = DB::table('assistants')->select('assistants.assistant_name', 'users.id')->join('users', 'assistants.user_id', '=', 'users.id')->where('users.id', $value->assistant_id)->first();

            $completeData[$key]['id']             = $value->id;
            $completeData[$key]['status']         = $value->status;
            $completeData[$key]['trip_name']      = DB::table('trips')->where('id', $value->trip_id)->lists('name')[0];
            $completeData[$key]['driver_id']      = $driverData->id;
            $completeData[$key]['driver_name']    = $driverData->driver_name;
            $completeData[$key]['assistant_id']   = $assistantData->id;
            $completeData[$key]['assistant_name'] = $assistantData->assistant_name;
            $completeData[$key]['truck_registration_number']     = DB::table('trucks')->where('id', $value->truck_id)->lists('registration_number')[0];
        }

        return view('mapping.lists', compact('completeData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View('mapping.create')
                ->with('trips', DB::table('trips')->lists('name', 'id'))
                ->with('trucks', DB::table('trucks')->lists('registration_number', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['driver_name', 'assistant_name']);
        $data['status']     = 0;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        DB::table('mapping')
                ->insert($data);

        return redirect('/mapping');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatus($id)
    {
        DB::table('mapping')
                ->where('id', $id)
                ->update(['status' => 1]);
    }
}
