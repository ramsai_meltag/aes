<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Input, Redirect;
class TripsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $trips = DB::table('trips')
                    ->select('trips.name', 'from.name as from', 'to.name as to', 'trips.id')
                    ->join('locations as from', 'trips.from', '=', 'from.id')
                    ->join('locations as to', 'trips.to', '=', 'to.id')
                    ->orderBy('trips.id', 'desc')->get();
        return View('trips.lists', compact('trips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View('trips.create')->with('locations', DB::table('locations')->where('status', 1)->lists('name', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data                   = Input::all();
        $data['created_at']     = date('Y-m-d H:i:s');
        $data['updated_at']     = date('Y-m-d H:i:s');

        DB::table('trips')
                    ->insert($data);

        return Redirect::to('trips')
                            ->with('message', 'Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $trip = DB::table('trips')->where('id', $id)->first();
        return View('trips.view', compact('trip'))->with('locations', DB::table('locations')->lists('name', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $trip = DB::table('trips')->where('id', $id)->first();
        return View('trips.edit', compact('trip'))->with('locations', DB::table('locations')->lists('name', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data                   = Input::except(['_method', '_token']);
        $data['updated_at']     = date('Y-m-d H:i:s');

        DB::table('trips')
                    ->where('id', $id)
                    ->update($data);

        return Redirect::to('trips')
                            ->with('message', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
