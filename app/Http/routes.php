<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
	
// });


Route::post('/fp/upload', function() {
    Log::info(Input::getContent());
    Log::info("=====");
    Log::info(Input::get('name'));
    $bytes_written = File::put(storage_path().'/isdb/'.Input::get('name'), Input::getContent());
    if ($bytes_written === false)
        return 0;

    return 1;
});

Route::get('/fp/download/{fileName}', function($fileName){
    return Response::download(storage_path().'/isdb/'.$fileName);
});

Route::get('/fp/user/{id}', function($id){
    Log::info($id);
});

Route::group(['middleware' => 'guest'], function () {
	Route::get('/', 'LoginController@login');
	Route::get('login', 'LoginController@login');
	Route::post('login', 'LoginController@loginUser');
});


Route::group(['middleware' => 'auth'], function () {
	Route::get('dashboard', 'DashboardController@showDashboard');
	Route::get('/create/user/{type}', 'DashboardController@createUser');
	Route::get('/get/user/{id}/{type}', 'DashboardController@getUser');
	Route::post('/change/trip/status/{id}', 'MappingController@changeStatus');
	Route::get('logout', 'LoginController@logoutUser');

	Route::resource('drivers', 'DriversController');
	Route::resource('assistants', 'AssistantsController');
	Route::resource('locations', 'LocationsController');
	Route::resource('trips', 'TripsController');
	Route::resource('trucks', 'TrucksController');
	Route::resource('mapping', 'MappingController');
});